package eu.gaiax.dataexchange.api;

import eu.gaiax.dataexchange.api.service.StorageService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@OpenAPIDefinition(
        info = @Info(
                title = "Data Exchange API",
                description = "Data Exchange Documentation",
                contact = @Contact(
                        name = "Van-Hoan Hoang",
                        email = "van-hoan.hoang@eviden.com"
                )
        )
)
public class ApiApplication {


    @Autowired
    private StorageService minioService;

    public static void main(String[] args) {
        SpringApplication.run(ApiApplication.class, args);
    }


}
