package eu.gaiax.dataexchange.api.rest.endpoint;

public interface Endpoints {
    String DATA_SET_BASE_PATH = "datasets";
    String FILE_BASE_PATH = "datasets/files";
}
