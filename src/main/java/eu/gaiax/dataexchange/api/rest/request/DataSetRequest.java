package eu.gaiax.dataexchange.api.rest.request;


import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DataSetRequest {


    @NotNull
    private Map<String, List<String>> request = new HashMap<>();
}
