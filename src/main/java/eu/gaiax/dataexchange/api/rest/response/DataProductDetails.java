package eu.gaiax.dataexchange.api.rest.response;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DataProductDetails {

    Map<String, List<String>> dataProduct = new HashMap<>();
}
