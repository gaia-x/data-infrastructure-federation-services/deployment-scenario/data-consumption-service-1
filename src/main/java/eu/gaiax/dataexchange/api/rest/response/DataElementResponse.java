package eu.gaiax.dataexchange.api.rest.response;


import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@NotNull
public class DataElementResponse {
    private String filename;
    private String url;
}
