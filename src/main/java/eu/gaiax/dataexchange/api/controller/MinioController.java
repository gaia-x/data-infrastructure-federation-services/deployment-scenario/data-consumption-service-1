package eu.gaiax.dataexchange.api.controller;


import eu.gaiax.dataexchange.api.rest.endpoint.Endpoints;
import eu.gaiax.dataexchange.api.rest.response.DataElementResponse;
import eu.gaiax.dataexchange.api.rest.response.DataProductDetails;
import eu.gaiax.dataexchange.api.service.StorageService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@RestController
@Tag(
        name = "Minio Data Exchange"
)
@RequestMapping("/api/v1")
@SecurityRequirement(
        name = "kc-authentication"
)
@Slf4j
@RequiredArgsConstructor
public class MinioController {


    private final StorageService minioService;

    @Operation(
            description = "List the details of datasets in the data product",
            summary = "List the details of datasets in the data product"
    )
    @GetMapping(Endpoints.DATA_SET_BASE_PATH)
    public ResponseEntity<?> getDataProductDetails(HttpServletRequest httpServletRequest){
        // List all the dataset and data items in each dataset
        String authorizationToken = httpServletRequest.getHeader("Authorization");
        String jwtToken = authorizationToken.replaceFirst("Bearer ", "");
        log.info("User accesses with JWT:  {}", jwtToken);

        // Extract token string from jwt token

        DataProductDetails details = minioService.listDataProductDetails(jwtToken);
        return new ResponseEntity<>(details, HttpStatus.OK);
    }

    @Operation(
            description = "Download an element from a dataset",
            summary = "Download an element from a dataset"
    )
    @GetMapping(Endpoints.DATA_SET_BASE_PATH + "/{datasetId}/{filename}")
    public ResponseEntity<?> retrieveDataSet(@PathVariable("datasetId") String dataSetId,
                                             @PathVariable("filename") String fileName,
                                             HttpServletRequest httpServletRequest
    )  {
        //String authorizationToken = httpServletRequest.getHeader("Authorization");
        //String jwtToken = authorizationToken.replaceFirst("Bearer ", "");
        String authorizationToken = httpServletRequest.getHeader("Authorization");
        String jwtToken = authorizationToken.replaceFirst("Bearer ", "");

        Resource resource = minioService.getDataElementInABucket(jwtToken, fileName, dataSetId);
        String contentType = null;
        try {
            contentType = httpServletRequest.getServletContext()
                    .getMimeType(resource.getFile().getAbsolutePath());
            log.info("Download file with content type {}", contentType);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

//    @Operation(
//            description = "Download a dataset",
//            summary = "Download a dataset"
//    )
//    @GetMapping(
//            value = Endpoints.RETRIEVE_DATA_SET + "/{datasetId}",
//            produces= "application/octet-stream"
//    )
//    public void retrieveDataProduct(@PathVariable("datasetId") String dataSetID,
//                                    HttpServletRequest httpServletRequest,
//                                    HttpServletResponse httpServletResponse){
//
//        //String jwtToken = authorizationToken.replace("Bearer ", "");
//        String authorizationToken = httpServletRequest.getHeader("Authorization");
//        String jwtToken = authorizationToken.replaceFirst("Bearer ", "");
//        String zipFilename = dataSetID;
//        try {
//            ZipOutputStream zipOutput = new ZipOutputStream(httpServletResponse.getOutputStream());
//
//            List<Resource> resources = minioService.getAllDataElementsInABucket(jwtToken, dataSetID);
//            for(Resource resource: resources){
//                ZipEntry zipEntry = new ZipEntry(resource.getFilename());
//                zipEntry.setSize(resource.contentLength());
//                zipOutput.putNextEntry(zipEntry);
//                StreamUtils.copy(resource.getInputStream(), zipOutput);
//            }
//            zipOutput.finish();
//            zipOutput.close();
//            httpServletResponse.setStatus(HttpServletResponse.SC_OK);
//            httpServletResponse.setContentType("application/zip");
//            httpServletResponse.addHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename \"" + zipFilename + "\"");
//        } catch (IOException e) {
//            log.error("Failed to zip files of the dataset {}", dataSetID);
//            throw new RuntimeException(e);
//        }
//    }


    @Operation(
            description = "Download all datasets in data product and store them onto the Backend",
            summary = "Download all datasets in data product and store them onto the Backend"
    )
    @GetMapping(
            value = Endpoints.DATA_SET_BASE_PATH + "/{datasetId}"
    )
    public ResponseEntity<?> retrieveDataProduct(@PathVariable("datasetId") String dataSetID,
                                    HttpServletRequest httpServletRequest){

        //String jwtToken = authorizationToken.replace("Bearer ", "");
        String authorizationToken = httpServletRequest.getHeader("Authorization");
        String jwtToken = authorizationToken.replaceFirst("Bearer ", "");

        List<DataElementResponse> resources = minioService.getAllDataElementsInABucket(jwtToken, dataSetID);

        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @Operation(
            description = "Download files which have already retrieved from Minio and stored onto the Backend",
            summary = "Download files which have already retrieved from Minio and stored onto the Backend"
    )
    @GetMapping(Endpoints.FILE_BASE_PATH + "/{filename}")
    public ResponseEntity<?> getFileInAPI(@PathVariable("filename") String filename,
                                          HttpServletRequest httpServletRequest){

        Resource resource = minioService.loadFileAsResource(filename);
        String contentType = null;

        try {
            contentType = httpServletRequest.getServletContext()
                    .getMimeType(resource.getFile().getAbsolutePath());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

}
