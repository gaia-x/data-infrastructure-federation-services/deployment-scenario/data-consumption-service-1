package eu.gaiax.dataexchange.api.service;


import eu.gaiax.dataexchange.api.rest.response.DataElementResponse;
import eu.gaiax.dataexchange.api.rest.response.DataProductDetails;
import io.minio.*;
import io.minio.credentials.ClientGrantsProvider;
import io.minio.credentials.Jwt;
import io.minio.errors.*;
import io.minio.messages.Bucket;
import io.minio.messages.Item;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Slf4j
public class MinioService implements StorageService{

    @Value("${service.storage.minio.url}")
    private String minioUrl;

    @Value("${service.storage.minio.admin.credentials.secret_key}")
    private String secretKey;

    @Value("${service.storage.minio.admin.credentials.access_key}")
    private String accessKey;


    @Value("${service.storage.minio.jwt.expiry}")
    private int jwtExpiry;

    private MinioClient getMinioClientInstanceWithExternalIDP(String jwt){
        Jwt jwtToken = new Jwt(jwt, jwtExpiry);
        //String stsEndpoint = "";
        ClientGrantsProvider clientGrantsProvider = new ClientGrantsProvider(
                () -> jwtToken,  minioUrl, 86000, null, null);

        MinioClient minioClient = MinioClient.builder()
                .endpoint(minioUrl)
                .credentialsProvider(clientGrantsProvider)
                .build();

        return minioClient;
    }

    private MinioClient getMinioClientInstanceWithCredentials(){
        MinioClient minioClient = MinioClient.builder()
                .endpoint(minioUrl)
                .credentials(accessKey, secretKey)
                .build();

        return minioClient;
    }


    @Override
    public List<String> listDataSetNames(String jwtToken) {
        //List names of all the datasets in the dataProduct

        List<String> bucketNames = new ArrayList<>();
        MinioClient minioClient = getMinioClientInstanceWithExternalIDP(jwtToken);
        try {
            List<Bucket> buckets = minioClient.listBuckets();
            for(Bucket bucket : buckets){
                bucketNames.add(bucket.name());
            }
            return bucketNames;

        } catch (ErrorResponseException | InsufficientDataException | InternalException | InvalidKeyException | InvalidResponseException | IOException | NoSuchAlgorithmException | ServerException | XmlParserException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public DataProductDetails listDataProductDetails(String jwtToken) {
        //List the details of the whole data product

        List<String> bucketNames = listDataSetNames(jwtToken);
        log.info("Getting bucket {}", Arrays.toString(bucketNames.toArray()));

        MinioClient minioClient = getMinioClientInstanceWithExternalIDP(jwtToken);
        Map<String, List<String>> datasetDetails = new HashMap<>();

        for(String bucketName: bucketNames){
            List<String> fileNames = new ArrayList<>();
            Iterable<Result<Item>> results = minioClient.listObjects(ListObjectsArgs
                    .builder()
                    .bucket(bucketName)
                    .build());

            results.forEach(result ->
            {
                try {
                    fileNames.add(result.get().objectName());
                } catch (ErrorResponseException | InsufficientDataException | InternalException | InvalidKeyException | InvalidResponseException | IOException | NoSuchAlgorithmException | ServerException | XmlParserException e) {
                    log.error("Failed to read file name from bucket");
                    throw new RuntimeException(e);
                }
            });

            datasetDetails.put(bucketName, fileNames);
        }

        return new DataProductDetails(datasetDetails);
    }

    @Override
    public Resource getDataElementInABucket(String jwtToken, String filename, String bucketId) {

        List<String> dataSetIds = listDataSetNames(jwtToken);
        if(!dataSetIds.contains(bucketId)){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You do not have permission to access the requested resource");
        }

        MinioClient  minioClient = getMinioClientInstanceWithCredentials();
        try{
            log.info("Filename is {} ", filename);
            String fileExtension = filename.substring(filename.lastIndexOf('.') - 1 );
            log.info("File extension is {}", fileExtension);
            String fileName = new SimpleDateFormat("yyyyMMddHHmmSS").format(new Date()) + fileExtension;
            log.info("Changing file with name {} to {} and saving it into the system", filename, fileName);
            minioClient.downloadObject(DownloadObjectArgs.builder()
                            .bucket(bucketId)
                            .object(filename)
                            .filename(fileName)
                    .build()
            );
            return loadFileAsResource(fileName);
        } catch (ErrorResponseException | InsufficientDataException | InternalException | InvalidKeyException | InvalidResponseException | IOException | NoSuchAlgorithmException | ServerException | XmlParserException e) {
            log.error("Failed to download document with name {} from Minio", filename );
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<DataElementResponse> getAllDataElementsInABucket(String jwtToken, String bucketId) {
        //List<String> dataList = listDataProductDetails(jwtToken);
        DataProductDetails dataProductDetails = listDataProductDetails(jwtToken);
        Set<String> bucketIds = dataProductDetails.getDataProduct().keySet();
        if(! bucketIds.contains(bucketId)){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You do not have permission to access the required resources");
        }
        List<String> filesInBucket = dataProductDetails.getDataProduct().get(bucketId);

        log.info("Getting all files {} in the bucket {}", Arrays.toString(filesInBucket.toArray()), bucketId);

        List<DataElementResponse> resources = new ArrayList<>();

        filesInBucket.stream().forEach(
                file -> {
                    Resource resource = getDataElementInABucket(jwtToken, file, bucketId);
                    String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentRequestUri()
                            .path("/files/")
                            .path(resource.getFilename())
                            .toUriString();
                    fileDownloadUri = fileDownloadUri.replace(bucketId + "/", "");
                    log.info("Downloaded file {} which can be downloaded from {}", file, fileDownloadUri);
                    DataElementResponse dataElementResponse = new DataElementResponse(file, fileDownloadUri);
                    resources.add(dataElementResponse);
                }
        );
        return resources;
    }

    @Override
    public Resource loadFileAsResource(String filename){
        try {
            File file = new File(filename);
            log.info("File uri:  {}", file.toURI().toString());
            Resource resource = new UrlResource(file.toURI());
            if(!resource.isFile()){
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                        String.format("Server failed to download file with name: %s", filename));
            } else {
                return resource;
            }
        } catch (MalformedURLException e) {
            log.error("Failed to load the file {} into resource", filename);
            throw new RuntimeException(e);
        }
    }
}
