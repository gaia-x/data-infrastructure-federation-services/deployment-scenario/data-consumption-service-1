package eu.gaiax.dataexchange.api.service;


import eu.gaiax.dataexchange.api.rest.response.DataElementResponse;
import eu.gaiax.dataexchange.api.rest.response.DataProductDetails;
import org.springframework.core.io.Resource;

import java.util.List;

public interface StorageService {


    List<String> listDataSetNames(String jwtToken);
    DataProductDetails listDataProductDetails(String jwtToken);
    Resource getDataElementInABucket(String jwtToken, String fileName, String bucketId);
    List<DataElementResponse> getAllDataElementsInABucket(String jwtToken, String bucketId);
    Resource loadFileAsResource(String filename);
}