## Data Consumption Service
The project is developed with the purpose of helping Data Consumer to access Datasets included in a previously signed Data Contract. In the context of the demo, all datasets are hosted on minio on the Data Provider side, the data consumption service is responsible for interacting with Minio to retrieve all the Datasets that a user is authorized to access.

The following diagram illustrates a general workflow between components:

![image](/documentation/architecture.png)


## Components
### Data Exchange Backend

The backend service plays three vital roles: 

- First, it receives the access_token from the frontend once the latter has successfully authenticated the user.
- Second, it exchanges the access token for a security token at Security Token Service (STS) which is part of Minio Service
- And lastly, it sends the security token to the Minio API to receives all the authorized datasets


### Data Exchange Frontend
For ease of interacting with the backend, a separate frontend component has been developed. 
The component will be in charge of the following roles:
- First, it authenticates user against IDP & its SSI Wallet to receive an access_token.
- Second, it requests all authorized datasets and display them on the UI by sending the access_token to the Backend.

## Build and Test
In order to build and test both components, two docker files ( [front](/ui/Dockerfile) and [back](Dockerfile) ) are provided for each of them.
You can also run the docker-compose file located at [here](docker-compose.yml) to avoid running two services separately.

By default, the two services are exposed on port 8085 and 3000, respectively.