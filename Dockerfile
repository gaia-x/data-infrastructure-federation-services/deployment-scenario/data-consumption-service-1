FROM eclipse-temurin:17-jdk-alpine

ARG JAR_FILE=/target/data-exchange-api-v1.0.jar
WORKDIR /app

COPY ${JAR_FILE} /app/data-exchange.jar

ENTRYPOINT ["java", "-jar", "data-exchange.jar"]