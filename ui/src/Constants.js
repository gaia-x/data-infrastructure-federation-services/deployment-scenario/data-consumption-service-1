const prod = {
  url: {
    KEYCLOAK_BASE_URL: "https://keycloak.demo23.gxfs.fr"
  }
}

const dev = {
  url: {
    KEYCLOAK_BASE_URL: "http://localhost:8081"
    //KEYCLOAK_BASE_URL: "http://host.docker.internal:8081"
  }
}

export const config = process.env.NODE_ENV === 'development' ? dev : prod