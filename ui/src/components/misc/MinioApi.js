import axios from 'axios'
import { config } from '../../Constants'

export const minioApi = {

  getDataProductDetails,
  getDataSetDetails,
  getFileFromBackend
}

// API for Data Exchange

function getDataProductDetails(token){
  try{
    return minioInstance.get(`/api/v1/datasets`, {
      headers: { 'Authorization': bearerAuth(token)}
    });
  } catch(error){
    console.log(`Got the error: ${error.message}`);
  }
  
}

function getDataSetDetails(datasetId, token){
  return minioInstance.get(`/api/v1/datasets/${datasetId}`, {
    headers: { 'Authorization': bearerAuth(token) }
  });
}

function getFileFromBackend(fileUrl, token){
  console.log(`Fileurl:  ${fileUrl}`)
  return axios.get(fileUrl, {
    headers: {
      'Authorization': bearerAuth(token),
    },
    responseType : 'blob'
  });
}



// -- Axios

const instance = axios.create({
  baseURL: config.url.API_BASE_URL
})

const minioInstance = axios.create({
  baseURL: "https://data-exchange.aster-x.demo23.gxfs.fr/backend"
})


instance.interceptors.response.use(response => {
  return response
}, function (error) {
  if (error.response.status === 404) {
    return { status: error.response.status }
  }
  return Promise.reject(error.response)
})

// -- Helper functions

function bearerAuth(token) {

  console.log(`Token is : ${token}`)
  return `Bearer ${token}`
}