import React, { useState } from 'react'
import {  Dropdown } from 'semantic-ui-react'

function DataProductForm({  handleChange, dataSetIds, handleSelectDataSetOption }) {


  console.log(`DatasetOptions is ${dataSetIds}`);

  return <Dropdown 
    button
    className='icon'
    labeled
    floating
    icon={'world'}
    options={dataSetIds}
   // options={languageOptions}
    onChange={(event, data) => handleChange(event, data)}
    search
    text='Select Dataset'
  />

}

export default DataProductForm